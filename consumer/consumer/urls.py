from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('records/total', views.total_records, name='total_records')
]