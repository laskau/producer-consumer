# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Record(object):
    def __init__(self, id, data, created_at, expires_at):
        self.id = id
        self.data = data
        self.created_at = created_at.strftime("%Y-%m-%dT%H:%M:%SZ")
        self.expires_at = expires_at.strftime("%Y-%m-%dT%H:%M:%SZ")
        