# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django import template
from consumer.models import Record
from .services import firebase_manager
from django.http import JsonResponse

def index(request):
    records_ref = list(firebase_manager.get_all_records())
    records = [Record(record.id, record.get('data'), record.get('created_at'), record.get('expires_at')) for record in records_ref]

    index_template = template.loader.get_template('index.html')
    context = {'records': records}
    return HttpResponse(index_template.render(context, request))

def total_records(request):
    records_ref = list(firebase_manager.get_all_records())
    return JsonResponse({'total': len(list(records_ref))})
