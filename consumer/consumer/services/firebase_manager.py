import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate('service_account.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

def get_all_records():
    return db.collection(u'records').order_by(u'created_at', direction=firestore.firestore.Query.DESCENDING).stream()
