var data = {
    datasets: [{
        label: "Total records ",
        borderColor: "rgba(26,183,89, 1)",
        backgroundColor: "rgba(64,221,127, 0.3)",
        borderWidth: 2
    }],
    labels: []
};

var options = {
maintainAspectRatio: false,
scales: {
    yAxes: [{
        stacked: true,
        gridLines: {
            display: true
        }, 
        ticks: {
            suggestedMax: 20
        }
    }],
    xAxes: [{
        gridLines: {
            display: true
        }
    }]
}
};

let chart = Chart.Line('chart', {
    options: options,
    data: data
});

setInterval(function() {
    $.get("http://" + window.location.host + "/records/total", function(data, status){
        if(status !== 'success'){
            
            alert('Server Error. Try Again, Please');
            return;
        }
        if (chart.data.labels.length == 7){
            chart.data.labels.shift(); 
            chart.data.datasets.forEach(dataset => {
                dataset.data.shift();
            });
        }
        let dateNow = new Date(); 
        let timeString = dateNow.getHours() + ':' + dateNow.getMinutes() + ':'+ dateNow.getSeconds();
        chart.data.labels.push(timeString); 
        chart.data.datasets.forEach(dataset => {
            dataset.data.push(data.total);
        });
        chart.update();
    });
}, 5000);