let filterIsOn = false; 
let switchIndex = false; 
window.onload = function connecToChannel(){
    const notificationSocket = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/notification_channel'
    );

    notificationSocket.onmessage = function(e) {
        if ($('#status-block').is(':visible')) {
            $('#status-block').hide()
        }
        const data = JSON.parse(e.data);

        if (data['message'] == 'insert'){
            addNewRecord(data); 
        } else if (data['message'] == 'delete'){
            deleteRecord(data); 
        }
    };

    notificationSocket.onclose = function(e) {
        console.error('Notification channel socket closed unexpectedly');
        let status = $('#status-block').show(); 
        setTimeout(function() {
            console.log('Trying to reconnect'); 
            connecToChannel();
        }, 1000);
    };
}

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, switchcount = 0;
    table = document.getElementById("records_table");
    switching = true;
    let sortingAsc = true; 

    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            if (sortingAsc) {
                if (Date.parse(x.innerHTML) > Date.parse(y.innerHTML)) {
                    shouldSwitch = true;
                    break;
                }
            } else {
                if (Date.parse(x.innerHTML) < Date.parse(y.innerHTML)) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount ++;
        } else if (switchcount == 0 && sortingAsc) {
            sortingAsc = false; 
            switching = true;
       }
    }

    switchIndex = !switchIndex; 
}

function filterByDate() {
    filterIsOn = true; 
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("dateFilter");
    if(!input.value) {
        filterIsOn = false;
    }

    filter = input.value.toUpperCase();
    table = document.getElementById("records_table");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }       
    }
}   

function addNewRecord(data){
    let tableRef = document.getElementById('records_table');
    let newRow = tableRef.insertRow(switchIndex ? -1 : 1);
    
    let idCell = newRow.insertCell(0);
    let dataCell = newRow.insertCell(1);
    let createdAtCell = newRow.insertCell(2);

    newRow.id = data['record_id'];
    newRow.style.display = filterIsOn ? "none" : ""; 
    idCell.appendChild(document.createTextNode(data['record_id']));
    dataCell.appendChild(document.createTextNode(data['data']));
    createdAtCell.appendChild(document.createTextNode(data['created_at']));
}

function deleteRecord(data){
    let tableRef = document.getElementById('records_table'); 
    let rowIndex = document.getElementById(data['record_id']).rowIndex; 
    
    tableRef.deleteRow(rowIndex); 
}