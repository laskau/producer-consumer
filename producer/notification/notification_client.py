import websockets
import json
import pickle
import sys
from notification import notification_builder

URI = f"ws://{sys.argv[0]}:8080/ws/notification_channel"
notification_builder = notification_builder.NotificationBuilder()

async def notify_insert(record):
    try:
        async with websockets.connect(URI) as websocket:
            notification = notification_builder.add_message_type('insert').add_record(record).notification
            notification.type = 'channel'

            await websocket.send(json.dumps(notification.to_dict()))
    except Exception as e:
        print(f'Error while sending notification [type: insert, record_id: {record.id}]')

async def notify_delete(record_id):
    try:
        async with websockets.connect(URI) as websocket:
            notification = notification_builder.add_message_type('delete').add_record_id(record_id).notification
            notification.type = 'channel'
            await websocket.send(json.dumps(notification.to_dict()))
    except Exception as e:
        print(f'Error while sending notification [type: delete, record_id: {record_id}]')
