import json
from json import JSONEncoder

class Notification():
    def __init__(self):
        self.message_type = 'unset'
    
    def to_dict(self):
        notification_dict = dict()
        if self.message_type == 'insert':
            notification_dict = {
                'type': 'notification',
                'message': 'insert',
                'record_id': self.record.id,
                'data': self.record.data,
                'created_at': self.record.created_at.strftime("%Y-%m-%dT%H:%M:%SZ"),
                'expires_at': self.record.expires_at.strftime("%Y-%m-%dT%H:%M:%SZ")
            }
        elif self.message_type == 'delete':
            notification_dict = {
                'type': 'notification',
                'message': 'delete',
                'record_id': self.record_id
            }
        return notification_dict

class NotificationEncoder(JSONEncoder):
    def default(self, notification):
        return notification.__dict__

class NotificationBuilder():
    def __init__(self):
        self.reset()

    @property
    def notification(self) -> Notification:
        notification = self._notification
        self.reset()
        return notification

    def reset(self):
        self._notification = Notification()
        return self

    def add_record_id(self, record_id) -> Notification:
        self._notification.record_id = record_id
        return self

    def add_record(self, record) -> Notification: 
        self._notification.record = record
        return self

    def add_message_type(self, message_type) -> Notification:
        self._notification.message_type = message_type
        return self
