from datetime import datetime, timedelta
from pytz import timezone
from record import Record
import string
import random
import time 
import schedule
from service_facade import ServiceFacade

services = ServiceFacade()

def generate_random_string():
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(32))

def insert_record():
    new_record = Record('', generate_random_string(), datetime.now(timezone('Europe/Moscow')), datetime.now(timezone('Europe/Moscow')) + timedelta(seconds=30))
    services.add_record(new_record)

def delete_old_records():
    services.delete_outdated_records()

schedule.every(2).seconds.do(insert_record)
schedule.every(1).second.do(delete_old_records)

while True:
    schedule.run_pending()
    time.sleep(1)
