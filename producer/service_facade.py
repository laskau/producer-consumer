import asyncio
from dao import firebase_client
from notification import notification_client

class ServiceFacade: 

    def add_record(self, record):
        # add try/catch
        firebase_client.add_document_to_collection(record)
        asyncio.get_event_loop().run_until_complete(notification_client.notify_insert(record))

    def delete_outdated_records(self):
        refs = firebase_client.get_removed_records()
        for ref in refs:
            asyncio.get_event_loop().run_until_complete(notification_client.notify_delete(ref))
            