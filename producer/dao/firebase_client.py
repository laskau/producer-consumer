from datetime import datetime
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json

cred = credentials.Certificate('service_account.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

def add_document_to_collection(record):
    doc_ref = db.collection('records').document()
    doc_ref.set(record.to_dict())
    record.id = doc_ref.id
    print(f'new record inserted with id: [{doc_ref.id}]')

def get_removed_records():
    old_records_refs = db.collection('records').where('expires_at', '<', datetime.utcnow()).stream()
    refs = []

    for record_ref in old_records_refs:
        refs.append(record_ref.id)
        db.collection(u'records').document(record_ref.id).delete()
        print(f'record with id [{record_ref.id}] was deleted')

    return refs
