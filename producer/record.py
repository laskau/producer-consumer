from __future__ import unicode_literals

class Record(object):
    def __init__(self, id, data, created_at, expires_at):
        self.id = id
        self.data = data
        self.created_at = created_at
        self.expires_at = expires_at

    def to_dict(self):
        record_dict = {
            'id': self.id,
            'data': self.data,
            'created_at': self.created_at,
            'expires_at': self.expires_at
        }

        return record_dict
