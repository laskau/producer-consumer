# Producer-Consumer Test Task
Привет! Данный репозиторий содержит код для задания, описанного в файле [EdgeVision - Task.pdf](/EdgeVision - Task.pdf)
Requirements: 
- docker-compose

# Configuration 
Для настройки системы перед запуском используйте файл [docker-compose.yml](/docker-compose.yml). 
Система включает в себя три сервиса: 
1. redis
`Для выбора порта измените redis -> ports -> "<port>:<port>"`
2. web (consumer)
`Для выбора порта измените command -> "0.0.0.0:<port>" и ports -> "<port>"`
3. worker (producer)
`Для выбора адрес consumer изменить command -> "python3 producer.py <consumer service name>"` 

При изменении порта redis убедитесь, что в файле [settings.py](/consumer/EdgeVisionTask/settings.py) 
значение `CHANNEL_LAYER` имеет такой же port как и в [docker-compose.yml](/docker-compose.yml).

# Running
Для того чтобы запустить проект выполните следующую команду в корневой директории: 
`docker-compose up`

Откройте в браузере адрес, указанный в конфигурации `web` [docker-compose.yml](/docker-compose.yml)